import sys
import re

def read_deck_file(path):
	with open(path) as file:
		lines = [
			line.strip()
				for line in file.readlines()
				if not line.isspace()
					and not line.startswith("//")
					and not line.startswith("#")
		]
		
		return lines

def extract_name_and_count(line):
	parts = re.match(r"(?:(\d+)\s?)?(.+)", line)
	
	if parts is None:
		return None
	
	count = int(parts.group(1))
	name = parts.group(2)
	
	return (name, count)

def extract_cards(lines):
	return [
		card
			for card in [extract_name_and_count(line) for line in lines]
			if card is not None
	]

def create_card_map(cards):
	card_map = {}
	
	for (name, count) in cards:
		if name in card_map:
			card_map[name] += count
		else:
			card_map[name] = count
	
	return card_map

def find_diff(map1, map2):
	diff = {}
	
	for (name, count) in map2.items():
		if name in diff:
			diff[name] += count
		else:
			diff[name] = count

	for (name, count) in map1.items():
		if name in diff:
			diff[name] -= count
		else:
			diff[name] = -count
	
	return diff

def main():
	if len(sys.argv) != 3:
		print("Two arguments required!")
		
		return
	
	path1 = sys.argv[1]
	path2 = sys.argv[2]
	lines1 = read_deck_file(path1)
	lines2 = read_deck_file(path2)
	cards1 = extract_cards(lines1)
	cards2 = extract_cards(lines2)
	map1 = create_card_map(cards1)
	map2 = create_card_map(cards2)
	diff = find_diff(map1, map2)
	added = [(name, count) for (name, count) in diff.items() if count > 0]
	removed = [(name, abs(count)) for (name, count) in diff.items() if count < 0]
	count_diff = sum([count for (_, count) in added]) - sum([count for (_, count) in removed])
	
	if added:
		print("Added:")
		
		for (name, count) in added:
			print("{} {}".format(count, name))
	
	if removed:
		if added: print()
		
		print("Removed:")
		
		for (name, count) in removed:
			print("{} {}".format(count, name))
	
	if added or removed: print()
	
	if count_diff != 0:
		print("{0:+d} cards in total".format(count_diff))
	else:
		print("Total number of cards unchanged")

if __name__ == "__main__":
	main()
